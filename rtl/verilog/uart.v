`default_nettype none

module uart (
    input wire          clk,
    input wire          arst,
    input wire          rx_bit,
    output reg          rx_data_valid,
    output reg [8:0]    rx_data,
    output reg          parity_err,
    output reg          frame_err,
    output reg          tx_bit,
    output reg          tx_data_valid,
    output reg [8:0]    tx_data,
    input wire [31:0]   configuration_vector,
    input wire [2:0]    num_data_bits,      // 0 for 5
                                            // 1 for 6
                                            // 2 for 7
                                            // 3 for 8
                                            // 4 for 9
                                            // 5-7 invalid
    input wire          use_parity,         // 1 for TRUE; 0 for FALSE
    input wire          parity_type,        // 1 for ODD; 0 for EVEN
    input wire          num_stop_bits,      // 0 for ONE; 1 for TWO
    input wire [3:0]    tx_clk_sample_edge  // determines which tx_clk number to
                                            // sample tx_bit on
);

    localparam PARITY_TYPE = 22;
    localparam USE_PARITY = 21;
    localparam NUM_STOP_BITS = 20;
    localparam NUM_DATA_BITS_MSB = 19;
    localparam NUM_DATA_BITS_LSB = 16;
    localparam RATE_SEL_MSB = 15;
    localparam RATE_SEL_LSB = 0;
    
    reg [15:0]  baud16x;
    reg         uartClkPulse;

    always @(posedge clk or posedge arst) begin
        if (arst) begin
            baud16x <= 0;
            uartClkPulse <= 0;
        end else begin
            if (baud16x == configuration_vector[RATE_SEL_MSB:RATE_SEL_LSB]) begin
                baud16x <= 0;
                uartClkPulse <= 1;
            end else begin
                baud16x <= baud16x + 1;
                uartClkPulse <= 0;
            end
        end
    end

    uart_rx uart_rx(
        .clk(clk),
        .arst(arst),
        .rx_clk(uartClkPulse),
        .rx_bit(rx_bit),
        .rx_data_valid(rx_data_valid),
        .rx_data(rx_data),
        .parity_err(parity_err),
        .frame_err(frame_err),
        .num_data_bits(configuration_vector[NUM_DATA_BITS_MSB:NUM_DATA_BITS_LSB]),
        .use_parity(configuration_vector[USE_PARITY]),
        .parity_type(configuration_vector[PARITY_TYPE]),
        .num_stop_bits(configuration_vector[NUM_STOP_BITS]),
        .rx_clk_sample_edge(rx_clk_sample_edge)
    );

    uart_tx uart_tx(
        .clk(clk),
        .arst(arst),
        .tx_clk(uartClkPulse),
        .tx_bit(tx_bit),
        .tx_data_valid(tx_data_valid),
        .tx_data(tx_data),
        .num_data_bits(configuration_vector[NUM_DATA_BITS_MSB:NUM_DATA_BITS_LSB]),
        .use_parity(configuration_vector[USE_PARITY]),
        .parity_type(configuration_vector[PARITY_TYPE]),
        .num_stop_bits(configuration_vector[NUM_STOP_BITS])
    );
endmodule
`default_nettype wire
