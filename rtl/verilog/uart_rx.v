//
// TODO 
// If the line is held in the logic low condition for longer than a character
// time, this is a break condition that can be detected by the UART.
//
`default_nettype none

module uart_rx (
    input wire          clk,
    input wire          arst,
    input wire          rx_clk,             // Nominally 16x desired baud rate
    input wire          rx_bit,
    output reg          rx_data_valid,
    output reg [8:0]    rx_data,
    output reg          parity_err,
    output reg          frame_err,
    output reg [3:0]    state,
    input wire [2:0]    num_data_bits,      // 0 for 5
                                            // 1 for 6
                                            // 2 for 7
                                            // 3 for 8
                                            // 4 for 9
                                            // 5-7 invalid
    input wire          use_parity,         // 1 for TRUE; 0 for FALSE
    input wire          parity_type,        // 1 for ODD; 0 for EVEN
    input wire          num_stop_bits,      // 0 for ONE; 1 for TWO
    input wire [3:0]    rx_clk_sample_edge  // determines which rx_clk number to
                                            // sample rx_bit on
);

    reg [3:0] rx_clk_cntr = 0;
    reg rx_bit_d1 = 0;
    reg parity = 0;
    wire rx_bit_edge;

    assign rx_bit_edge = rx_bit ^ rx_bit_d1;

    always @(posedge clk or posedge arst) begin
        if (arst) begin
            rx_clk_cntr <= 0;
            rx_bit_d1 <= 1'b0;
        end else begin
            rx_bit_d1 <= rx_bit;

            if (rx_bit_edge) begin
                rx_clk_cntr <= 0;
            end else begin
                if (rx_clk) begin
                    rx_clk_cntr <= rx_clk_cntr + 1;
                end
            end
        end
    end

    // FSM to rx UART data
    parameter RX_IDLE = 0;
    parameter RX_START = 1;
    parameter RX_DATA0 = 2;
    parameter RX_DATA1 = 3;
    parameter RX_DATA2 = 4;
    parameter RX_DATA3 = 5;
    parameter RX_DATA4 = 6;
    parameter RX_DATA5 = 7;
    parameter RX_DATA6 = 8;
    parameter RX_DATA7 = 9;
    parameter RX_DATA8 = 10;
    parameter RX_DATA9 = 11;
    parameter RX_PAR = 12;
    parameter RX_STOP1 = 13;
    parameter RX_STOP2 = 14;
    parameter RX_DATA_VALID = 15;

    reg [3:0] rx_state = 0;
    reg [3:0] rx_state_next = 0;

    always @(posedge clk or posedge arst) begin
        if(arst) begin
            rx_state <= RX_IDLE;
            rx_data_valid <= 1'b0;
            parity_err <= 1'b0;
            frame_err <= 1'b0;
        end else begin
            rx_data_valid <= 1'b0;
            case(rx_state)
                RX_IDLE: begin
                    parity <= 1'b0;
                    parity_err <= 1'b0;
                    //frame_err <= 1'b0;
                    //state <= 4'hF;


                    //if (rx_bit_edge && ~rx_bit) begin
                    if (~rx_bit) begin
                        rx_state <= RX_START;
                    end
                end
                RX_START: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        if (~rx_bit) begin
                            rx_state <= RX_DATA0;
                            frame_err <= 1'b0;
                        end else begin
                            frame_err <= 1'b1;
                            rx_state <= RX_IDLE;
                        end
                    end
                end
                RX_DATA0: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;
                        rx_state <= RX_DATA1;
                    end
                end
                RX_DATA1: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;
                        rx_state <= RX_DATA2;
                    end
                end
                RX_DATA2: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;
                        rx_state <= RX_DATA3;
                    end
                end
                RX_DATA3: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;
                        rx_state <= RX_DATA4;
                    end
                end
                RX_DATA4: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;

                        if (num_data_bits == 0) begin
                            if (use_parity) begin
                                rx_state <= RX_PAR;
                            end else begin
                                rx_state <= RX_STOP1;
                            end
                        end else begin
                            rx_state <= RX_DATA5;
                        end
                    end
                end
                RX_DATA5: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;

                        if (num_data_bits == 1) begin
                            if (use_parity) begin
                                rx_state <= RX_PAR;
                            end else begin
                                rx_state <= RX_STOP1;
                            end
                        end else begin
                            rx_state <= RX_DATA6;
                        end
                    end
                end
                RX_DATA6: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;

                        if (num_data_bits == 2) begin
                            if (use_parity) begin
                                rx_state <= RX_PAR;
                            end else begin
                                rx_state <= RX_STOP1;
                            end
                        end else begin
                            rx_state <= RX_DATA7;
                        end
                    end
                end
                RX_DATA7: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;

                        if (num_data_bits == 3) begin
                            if (use_parity) begin
                                rx_state <= RX_PAR;
                            end else begin
                                rx_state <= RX_STOP1;
                            end
                        end else begin
                            rx_state <= RX_DATA8;
                        end
                    end
                end
                RX_DATA8: begin
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        rx_data <= {rx_bit, rx_data[8:1]};
                        parity <= parity ^ rx_bit;

                        if (use_parity) begin
                            rx_state <= RX_PAR;
                        end else begin
                            rx_state <= RX_STOP1;
                        end
                    end
                end
                RX_PAR: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        if (rx_bit != parity) begin
                            parity_err <= 1'b1;
                        end

                        rx_state <= RX_STOP1;
                    end
                end
                RX_STOP1: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        if (~rx_bit) begin
                            frame_err <= 1'b1;
                            rx_state <= RX_IDLE;
                        end else begin
                            if (num_stop_bits) begin
                                rx_state <= RX_STOP2;
                            end else begin
                                rx_state <= RX_DATA_VALID;
                            end
                            
                        end
                    end
                end
                RX_STOP2: begin
                    state <= rx_state;
                    if (rx_clk && (rx_clk_cntr == rx_clk_sample_edge)) begin
                        if (~rx_bit) begin
                            frame_err <= 1'b1;
                            rx_state <= RX_IDLE;
                        end else begin
                            rx_state <= RX_DATA_VALID;
                        end
                    end
                end
                RX_DATA_VALID: begin
                    state <= rx_state;
                    rx_data_valid <= 1'b1;

                    rx_state <= RX_IDLE;

                    //state <= 4'hf;
                    case (num_data_bits)
                        0: begin
                            rx_data <= {4'b0, rx_data[8:4]};
                        end
                        1: begin
                            rx_data <= {3'b0, rx_data[8:3]};
                        end
                        2: begin
                            rx_data <= {2'b0, rx_data[8:2]};
                        end
                        3: begin
                            rx_data <= {1'b0, rx_data[8:1]};
                        end
                        default: begin
                            rx_data <= rx_data;
                        end
                    endcase
                end
                default: $display("BAD STATE");
            endcase
        end
    end
endmodule

`default_nettype wire
