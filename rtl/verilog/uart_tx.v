`default_nettype none

module uart_tx (
    input wire          clk,
    input wire          arst,
    input wire          tx_clk,             // Nominally 16x desired baud rate
    output reg          tx_bit,
    output reg          tx_data_valid,
    output reg [8:0]    tx_data,
    input wire [2:0]    num_data_bits,      // 0 for 5
                                            // 1 for 6
                                            // 2 for 7
                                            // 3 for 8
                                            // 4 for 9
                                            // 5-7 invalid
    input wire          use_parity,         // 1 for TRUE; 0 for FALSE
    input wire          parity_type,        // 1 for ODD; 0 for EVEN
    input wire          num_stop_bits       // 0 for ONE; 1 for TWO
);

    reg [3:0] tx_clk_cntr = 0;
    reg tx_bit_d1 = 0;
    reg parity = 0;

    always @(posedge clk or posedge arst) begin
        if (arst) begin
            tx_clk_cntr <= 0;
            tx_bit_d1 <= 1'b0;
        end else begin
            tx_bit_d1 <= tx_bit;

            if (tx_bit_edge) begin
                tx_clk_cntr <= 0;
            end else begin
                if (tx_clk) begin
                    tx_clk_cntr <= tx_clk_cntr + 1;
                end
            end
        end
    end

    // FSM to rx UART data
    parameter TX_IDLE = 0;
    parameter TX_START = 1;
    parameter TX_DATA0 = 2;
    parameter TX_DATA1 = 3;
    parameter TX_DATA2 = 4;
    parameter TX_DATA3 = 5;
    parameter TX_DATA4 = 6;
    parameter TX_DATA5 = 7;
    parameter TX_DATA6 = 8;
    parameter TX_DATA7 = 9;
    parameter TX_DATA8 = 10;
    parameter TX_DATA9 = 11;
    parameter TX_PAR = 12;
    parameter TX_STOP1 = 13;
    parameter TX_STOP2 = 14;
    parameter TX_DATA_VALID = 15;

    reg [3:0] tx_state = 0;
    reg [3:0] tx_state_next = 0;

    always @(posedge clk or posedge arst) begin
        if(arst) begin
            tx_state <= TX_IDLE;
            tx_data_valid <= 1'b0;
        end else begin
            tx_data_valid <= 1'b0;
            case(tx_state)
                TX_IDLE: begin
                    parity <= 1'b0;


                    //if (tx_bit_edge && ~tx_bit) begin
                    if (~tx_bit) begin
                        tx_state <= TX_START;
                    end
                end
                TX_START: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        if (~tx_bit) begin
                            tx_state <= TX_DATA0;
                        end else begin
                            tx_state <= TX_IDLE;
                        end
                    end
                end
                TX_DATA0: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;
                        tx_state <= TX_DATA1;
                    end
                end
                TX_DATA1: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;
                        tx_state <= TX_DATA2;
                    end
                end
                TX_DATA2: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;
                        tx_state <= TX_DATA3;
                    end
                end
                TX_DATA3: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;
                        tx_state <= TX_DATA4;
                    end
                end
                TX_DATA4: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;

                        if (num_data_bits == 0) begin
                            if (use_parity) begin
                                tx_state <= TX_PAR;
                            end else begin
                                tx_state <= TX_STOP1;
                            end
                        end else begin
                            tx_state <= TX_DATA5;
                        end
                    end
                end
                TX_DATA5: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;

                        if (num_data_bits == 1) begin
                            if (use_parity) begin
                                tx_state <= TX_PAR;
                            end else begin
                                tx_state <= TX_STOP1;
                            end
                        end else begin
                            tx_state <= TX_DATA6;
                        end
                    end
                end
                TX_DATA6: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;

                        if (num_data_bits == 2) begin
                            if (use_parity) begin
                                tx_state <= TX_PAR;
                            end else begin
                                tx_state <= TX_STOP1;
                            end
                        end else begin
                            tx_state <= TX_DATA7;
                        end
                    end
                end
                TX_DATA7: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;

                        if (num_data_bits == 3) begin
                            if (use_parity) begin
                                tx_state <= TX_PAR;
                            end else begin
                                tx_state <= TX_STOP1;
                            end
                        end else begin
                            tx_state <= TX_DATA8;
                        end
                    end
                end
                TX_DATA8: begin
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        tx_data <= {tx_bit, tx_data[8:1]};
                        parity <= parity ^ tx_bit;

                        if (use_parity) begin
                            tx_state <= TX_PAR;
                        end else begin
                            tx_state <= TX_STOP1;
                        end
                    end
                end
                TX_PAR: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        if (tx_bit != parity) begin
                        end

                        tx_state <= TX_STOP1;
                    end
                end
                TX_STOP1: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        if (~tx_bit) begin
                            tx_state <= TX_IDLE;
                        end else begin
                            if (num_stop_bits) begin
                                tx_state <= TX_STOP2;
                            end else begin
                                tx_state <= TX_DATA_VALID;
                            end
                            
                        end
                    end
                end
                TX_STOP2: begin
                    state <= tx_state;
                    if (tx_clk && (tx_clk_cntr == tx_clk_sample_edge)) begin
                        if (~tx_bit) begin
                            tx_state <= TX_IDLE;
                        end else begin
                            tx_state <= TX_DATA_VALID;
                        end
                    end
                end
                TX_DATA_VALID: begin
                    state <= tx_state;
                    tx_data_valid <= 1'b1;

                    tx_state <= TX_IDLE;

                    case (num_data_bits)
                        0: begin
                            tx_data <= {4'b0, tx_data[8:4]};
                        end
                        1: begin
                            tx_data <= {3'b0, tx_data[8:3]};
                        end
                        2: begin
                            tx_data <= {2'b0, tx_data[8:2]};
                        end
                        3: begin
                            tx_data <= {1'b0, tx_data[8:1]};
                        end
                        default: begin
                            tx_data <= tx_data;
                        end
                    endcase
                end
                default: $display("BAD STATE");
            endcase
        end
    end
endmodule

`default_nettype wire
