
`timescale 1ns/1ps

`define assert_true(cond, message) \
        if (!(cond)) begin \
            $display("ASSERTION FAILED in %m at ", $time); \
            $display((message)); \
            PrintStats; \
            $finish_and_return(-1); \
        end

module uart_rx_tb();

    reg clk = 0;
    reg uart_tx_clk = 0;
    reg arst = 1;
    reg rx_clk = 0;
    reg rx_bit = 1;
    wire        rx_data_valid;
    wire [8:0]  rx_data;
    wire        parity_err;
    wire        frame_err;
    wire [3:0]  state;
    reg  [2:0]  num_data_bits = 3'd3;
    reg         use_parity = 1'b0;
    reg         parity_type = 1'b1;
    reg         num_stop_bits = 1'b0;
    reg  [3:0]  rx_clk_sample_edge = 4'h7;

    uart_rx
    uart_rx_dut(
        .clk(clk),
        .arst(arst),
        .rx_clk(cycle_cnt == 6),
        .rx_bit(rx_bit),
        .rx_data_valid(rx_data_valid),
        .rx_data(rx_data),
        .parity_err(parity_err),
        .frame_err(frame_err),
        .state(state),
        .num_data_bits(num_data_bits),
        .use_parity(use_parity),
        .parity_type(parity_type),
        .num_stop_bits(num_stop_bits),
        .rx_clk_sample_edge(rx_clk_sample_edge)
    );

    real clk_halfperiod = 41;
    real uart_tx_clk_halfperiod = 4340;
    real uart_rx_clk_halfperiod = 542;

    initial begin
        forever begin
            #clk_halfperiod; clk = 1;
            #clk_halfperiod; clk = 0;
            #clk_halfperiod; clk = 1;
            #clk_halfperiod; clk = 0;
            #clk_halfperiod; clk = 1; arst = 0;
            #clk_halfperiod; clk = 0;
        end
    end

    initial begin
        forever begin
            #uart_tx_clk_halfperiod; uart_tx_clk = 1;
            #uart_tx_clk_halfperiod; uart_tx_clk = 0;
        end
    end

    reg [15:0] cycle_cnt = 0;

    always @(posedge clk) begin
        if (cycle_cnt == 6)
            cycle_cnt <= 0;
        else
            cycle_cnt <= cycle_cnt + 1;
    end

    initial begin
        forever begin
            #uart_rx_clk_halfperiod; rx_clk = 1;
            #uart_rx_clk_halfperiod; rx_clk = 0;
        end
    end

    task TxUartData(
        input integer tx_data,
        input integer num_data_bits,
        input integer use_parity,
        input integer parity_type,
        input integer num_stop_bits
    );
        integer i;
    begin
        wait(~uart_tx_clk);
        wait(uart_tx_clk);
        rx_bit = 0;
        wait(~uart_tx_clk);
        wait(uart_tx_clk);
        for(i = 0; i < 8; i = i +1) begin
            rx_bit = tx_data[i];
            wait(~uart_tx_clk);
            wait(uart_tx_clk);
        end
        rx_bit = 1;
    end
    endtask

    initial begin
        wait(~arst);
        wait(uart_tx_clk);
        TxUartData(5, 8, 0, 0, 1);
        #10000
        $finish();
    end
    initial begin
        $dumpfile("test.vcd");
        $dumpvars(0,uart_rx_tb);
    end

endmodule
